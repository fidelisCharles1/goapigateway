package URLS

import (
	"fmt"
	"encoding/json"
    KeyValues "GOAPIGateway/KeyValues"
)


var HeadersData []KeyValues.KeyValueSettings

func ProcessHTTPHeaders(headers []KeyValues.KeyValueSettings,headersFromConfig string)([]KeyValues.KeyValueSettings){
    fmt.Println("Processing HTTP Headers")

    err := json.Unmarshal([]byte(headersFromConfig),&headers)
    if(err!=nil){
        fmt.Println(" - Error reading http headers")
        fmt.Println(err)
    }

    return headers
}
