package JWT

import (
    "os"
    "fmt"
    "time"
    "errors"
    "strconv"
    "encoding/json"
    "github.com/dgrijalva/jwt-go"
)

type JWT struct{
    Secret string `json:"secret"`
    //this is header to look for the token for
    TokenHeader string `json:"tokenHeader"`
    TimeToExpire string `json:"timeToExpire"`
}

func InitializeJWT(jwt JWT,jwtConfigurations string)(JWT,error){
    fmt.Println("Processing JWT configurations")
    var err error
    var timeToExpire int

    err = json.Unmarshal([]byte(jwtConfigurations),&jwt)
    if(err!=nil){
        goto ret
    }

    if(jwt.Secret!=""){

        timeToExpire,err = strconv.Atoi(jwt.TimeToExpire)
        if(err!=nil){
            goto ret
        }

        if(timeToExpire<=0){
            err = errors.New("JWT Token's time to expire must be greater than zero")
            goto ret
        }

    }else{
        jwt.TimeToExpire = ""
    }

    ret:
    return jwt,err
}

func ValidateToken(tokenString,secret string)(bool,error){
    var passed bool
    var token *jwt.Token
    var err error
    // Parse takes the token string and a function for looking up the key. The latter is especially
    // useful if you use multiple keys for your application.  The standard is to use 'kid' in the
    // head of the token to identify which key to use, but the parsed token (head and claims) is provided
    // to the callback, providing flexibility.
    token, err = jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
        // Don't forget to validate the alg is what you expect:
        if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
            return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
        }

        return []byte(secret), nil
    })

    if(token==nil){
        err = errors.New("A JWT token is missing or an invalid token was supplied")
        goto ret
    }

    if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
        passed = true
    } else {
        goto ret
    }    

    ret:
    return passed,err
}

/*
    SET UP FOR FUTURE USER CASES
*/
func (jwtPayload *JWT)CreateToken()(string,error){
     var err error
     var token string
     var at *jwt.Token
     var timeOut int
     var atClaims jwt.MapClaims

     timeOut,err = strconv.Atoi(jwtPayload.TimeToExpire)
      if err != nil {
        goto ret
      }

      //Creating Access Token
      os.Setenv("ACCESS_SECRET", jwtPayload.Secret) 
      atClaims = jwt.MapClaims{}
      atClaims["authorized"] = true
      atClaims["exp"] = time.Now().Add(time.Minute * time.Duration(timeOut)).Unix()

      at = jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
      token, err = at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
      if err != nil {
        goto ret
      }

      ret:
      return token,err
}