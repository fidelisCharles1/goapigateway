package Network

import (
    "time"
    "bytes"
    "net/http"
    "io"
    "io/ioutil"
    "compress/gzip"
    "crypto/tls"
)

type ErrorResponse struct{
    Code int `json:"code"`
    Message string `json:"message"`
    Body interface{} `json:"payload"`
}

func Network(url ,method string,requestTimeOuts int,payload []byte,requestHeaders map[string]string)(*http.Response,error){
    var res *http.Response
    var req *http.Request
    var err error

    client:=&http.Client{
        Transport: &http.Transport{
            TLSClientConfig: &tls.Config{
                InsecureSkipVerify: true,
            },
        },
    }

    client.Timeout = time.Second * time.Duration(requestTimeOuts)
    req, err = http.NewRequest(method, url, bytes.NewBuffer(payload))
    if(err!=nil){
        goto ret
    }

    req.Close = true
    for key,val := range requestHeaders{
        req.Header.Add(key, val)
    }
    

    res,err = client.Do(req)
    if(err!=nil){
        goto ret
    }

    ret:
    return res,err
}

func ReadGZIPCompressedResponse(body io.Reader)([]byte,error){
    var bts []byte
    var err error

    reader,err := gzip.NewReader(body)
    if(err!=nil){
        goto ret
    }

    defer reader.Close()

    bts,err = ioutil.ReadAll(reader)
    if(err!=nil){
        goto ret
    }

    ret:
    return bts,err
}

func GetRequestIP(r *http.Request) string {
    forwarded := r.Header.Get("X-FORWARDED-FOR")
    if forwarded != "" {
        return forwarded
    }
    return r.RemoteAddr
}