package main

import (
    KeyValues "GOAPIGateway/KeyValues"
    Headers "GOAPIGateway/Headers"
    Network "GOAPIGateway/Network"
    URLS "GOAPIGateway/URLS"
    SSL "GOAPIGateway/SSL"
    JWT "GOAPIGateway/JWT"
    Files "GOAPIGateway/Files"
    FileHandler "GOAPIGateway/FileHandler"

    "encoding/json"
    "io/ioutil"
    "net/http"
    "strconv"
    "strings"
    "errors"
    "time"
    "log"
    "fmt"
    "os"
)

var file,logFile *os.File
var err error
var bts []byte
var configurations map[string]interface{}
var requestTimeOuts,sizeLimit int
var port,logsDirectory,logFileName,StorageDirectory string
var mappedUrls map[string]URLS.URL
var headers []KeyValues.KeyValueSettings
var ssl SSL.SSLPayload
var responseHeadersToSkip map[string]string = map[string]string{"Content-Encoding":""}
var jwt JWT.JWT
/*
    Here we are reading content of the configuration file and setup the system as per the requirements
*/
func initialize()error{
    fmt.Println("Initializing Gateway")

    file, err = os.Open("conf.json")
    if err != nil {
        goto ret
    }

    bts, err = ioutil.ReadAll(file)
    err = json.Unmarshal(bts,&configurations)

    for key,val := range configurations{
        bts,_ = json.Marshal(val)
        switch(key){

        case "port":
            port = strings.Trim(string(bts),"\"")
            break

        case "url":
            mappedUrls = URLS.ProcessUrls(mappedUrls, string(bts))
            break

        case "SSL":
            ssl = SSL.ProcessSSL(ssl,string(bts))
            break

        case "HTTPHeaders":
            headers = Headers.ProcessHTTPHeaders(headers,string(bts))
            break

        case "requestTimeOuts":
            requestTimeOuts,err = strconv.Atoi( strings.Trim(string(bts),"\"") )
            if(err!=nil){
                goto ret
            }
            if(requestTimeOuts<=0){
                err = errors.New("Request timeouts must be greater than 0")
                goto ret
            }
            break

        case "JWT":
            jwt,err = JWT.InitializeJWT(jwt,string(bts))
            if(err!=nil){
                goto ret
            }
            break

        case "logsDirectory":
            logsDirectory = strings.Trim(string(bts),"\"")
            logFile,logFileName,err = Files.SetUpLogEnviroment(logsDirectory)
            if(err!=nil){
                goto ret
            }
            break

        case "storageDirectory":
            StorageDirectory = strings.Trim(string(bts),"\"")
            err = Files.SetUpStorageEnviroment(StorageDirectory)
            if(err!=nil){
                goto ret
            }
            break

        case "sizeLimit":
            sizeLimit,err = strconv.Atoi( strings.Trim(string(bts),"\"") )
            if(err!=nil){
                goto ret
            }
            break

        default:
            break
        }
    }

    file.Close()   

    ret:
    return err
}



/*
    Enable Cors, for browsers and other devices that have this setup this enbles applications to not break
*/

func enableCors(w *http.ResponseWriter,r *http.Request) {

    for x := range headers{
        (*w).Header().Set(headers[x].SettingName, headers[x].SettingValue)
    }

    var host string = "*"

    if header ,ok := r.Header["Origin"]; ok{
        host = header[0]
    }

    (*w).Header().Set("Access-Control-Allow-Origin", host)
}


func ErrorResponse(errorCode int,errorMessage string)[]byte{
    var bts []byte
    var response Network.ErrorResponse

    response.Code = errorCode
    response.Message = errorMessage

    bts,_ = json.Marshal(response)

    return bts
}

func treatANDSignal(parameter string)string{
    return strings.Replace(parameter,"%20&%20","_1AND1_",-1)
}


func processRequest(incomingURL URLS.URL, w http.ResponseWriter, r *http.Request){
    var methods = strings.Split(incomingURL.Method,",")
    var methodMatched,requestSucceeded bool
    var finalUrl string = incomingURL.Target
    var requestSource string
    var payload,responseData []byte
    var responseHeaders http.Header
    var res *http.Response

    requestSource = Network.GetRequestIP(r)

    var urlParameters []string
    urlParameters = strings.Split(r.RequestURI,"?")

    if(len(urlParameters)>1){
        if( len(strings.Split(incomingURL.Target,"?"))> 1){
            finalUrl += "&"+treatANDSignal(urlParameters[1])
        }else{
            finalUrl += "?"+treatANDSignal(urlParameters[1])
        }
    }

    var requestHeaders map[string]string = map[string]string{}
    for key,val := range r.Header{
        requestHeaders[key] = val[0]
    }
    requestHeaders["Accept-Encoding"] = "gzip,identity"


    for y := range methods{
        if(strings.ToUpper(methods[y])==r.Method){
            methodMatched = true
            break
        }
    }

    if(methodMatched==true){

        var tokenValidated bool

        payload,err = ioutil.ReadAll(r.Body)
        if(err!=nil){

            responseData = ErrorResponse(416,err.Error())
            goto exitCall

        }

        if( strings.ToUpper(incomingURL.Secured) == "TRUE"){
            tokenValidated,err = JWT.ValidateToken(r.Header.Get(jwt.TokenHeader),jwt.Secret)
            if(err!=nil){
                responseData = ErrorResponse(401,err.Error())
                goto exitCall
            }
            if(!tokenValidated){
                responseData = ErrorResponse(401,"Your JWT token is invalid")
                goto exitCall
            }
        }

        // log.Println("FINAL URL "+finalUrl)

        res,err = Network.Network(finalUrl,r.Method,requestTimeOuts,payload,requestHeaders)
        if(err!=nil){

            responseData = ErrorResponse(416,err.Error())
            goto exitCall

        }


        if(res.Header.Get("Content-Encoding")=="gzip"){
            responseData,err = Network.ReadGZIPCompressedResponse(res.Body)
        }else{
            responseData,err = ioutil.ReadAll(res.Body)
        }

        if(err!=nil){

            responseData = ErrorResponse(res.StatusCode,err.Error())
            goto exitCall

        }

        responseHeaders = res.Header
        for key,value := range res.Header{
            if _,ok := responseHeadersToSkip[key]; !ok{
                (w).Header().Set(key, value[0])
            }
        }
//        (w).Header().Set("Content-Length", strconv.Itoa( len( string(bts) ) ) )
        requestSucceeded = true
        goto exitCall

    }else{
        responseData = ErrorResponse(404,"The method does not match that of the configured url")
        goto exitCall
    }    

    exitCall:
    go func(){
      var bts []byte
        bts,_ = json.Marshal(incomingURL)
        processAndSaveLog( payload, responseData, bts, requestSource, r.Method,r.Header,responseHeaders,requestSucceeded)
    }()

    if(res!=nil){
        res.Body.Close()
    }
    
    fmt.Fprintf(w,string(responseData))
}

func processErrorRequest(w http.ResponseWriter, r *http.Request){
    var payload,responseData []byte
    var requestSource string
    var err error
    requestSource = Network.GetRequestIP(r)

    payload,err = ioutil.ReadAll(r.Body)
    if(err!=nil){

        responseData = ErrorResponse(416,err.Error())
        goto exitCall

    }

    responseData = ErrorResponse(404,"The end point you are trying to reach is unavailable")

    exitCall:
    go func(){
      var incomingURL URLS.URL
      var bts []byte
      incomingURL.Source = r.RequestURI
      bts,_ = json.Marshal(incomingURL)
        processAndSaveLog( payload, responseData, bts, requestSource,r.Method,r.Header,nil, false)
    }()

    fmt.Fprintf(w,string(responseData))

}

func processAndSaveLog(payload,response,gatewayData []byte,sourceIP,method string,
        requestHeaders,responseHeaders http.Header,requestSucceeded bool){
    var logTime = time.Now().Format("2006-01-02 15:04:05")

    // var fileName = "access-logs-"+time.Now().Format("2006-01-02")
    // fileName += ".txt"

    // //if the logfile names are not similar then it means a different day has started,
    // //we hence close the previous file and pull create a new one
    // if(fileName != logFileName){
    //     //close the previous file
    //     logFile.Close()
    //     //set up a new file
    //     logFile,logFileName,err = Files.SetUpLogEnviroment(logsDirectory)
    // }

    var requestHeaderBytes,responseHeaderBytes []byte

    requestHeaderBytes,_ = json.Marshal(requestHeaders)
    responseHeaderBytes,_ = json.Marshal(responseHeaders)

    var logData = "Time : "+logTime
    logData += "\r\nSource IP : "+sourceIP
    logData += "\r\nMethod : "+method
    logData += "\r\nRequest Headers : "+string(requestHeaderBytes)
    logData += "\r\nResponse Headers : "+string(responseHeaderBytes)
    logData += "\r\nGateway Data : "+string(gatewayData)
    logData += "\r\nPayload : "+string(payload)
    logData += "\r\nResponse : "+string(response)
    if(requestSucceeded){
        logData += "\r\nStatus : SUCCEEDED"
    }else{
        logData += "\r\nStatus : FAILED"
    }

    Files.DataPool = append(Files.DataPool,logData)

}


func main(){

    var err error
    err = initialize()
    if(err!=nil){
        fmt.Println("Could not start gateway")
        fmt.Println(err)
        return
    }
    defer logFile.Close()
 


    for x := range mappedUrls{

        tmp := mappedUrls[x]

        http.HandleFunc(tmp.Source,func(w http.ResponseWriter, r *http.Request){
            enableCors(&w,r)
            processRequest(tmp,w,r)
        })

    }

    /*
    Because the gateway sits in front of the application and we do not have an external storage facility,
    We have decided to push files here through the application server, hence an additional function to handle this
    */
    http.HandleFunc("/store-file", func(w http.ResponseWriter, r *http.Request){
        enableCors(&w,r)
        FileHandler.SaveFile(StorageDirectory,w,r)
    })


    //this is created to handle 404 errors, we need to log these so we can gauge how and if the platform is receiving 
    //requests from external parties
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request){
        enableCors(&w,r)
        processErrorRequest(w,r)
    })


    http.HandleFunc("/health-check",func(w http.ResponseWriter,r *http.Request){
        enableCors(&w,r)
        var response Network.ErrorResponse
        var bts []byte

        response.Code = 200
        response.Message = "Service is healthy"

        bts,_ = json.Marshal(response)

        fmt.Fprintf(w,string(bts))
    })



    fmt.Println("Persistance loading sequence initialized")

    go Files.PersistanceLoading(logFile,&sizeLimit,&logsDirectory,&logFileName)


    srv := &http.Server{
        Addr:         ":"+port,
        ReadTimeout:  time.Duration(requestTimeOuts) * time.Second,
        WriteTimeout: time.Duration(requestTimeOuts) * time.Second,
    }


    if(ssl.SSLFullChainPath != "" && ssl.SSLPrivateKeyPath != ""){
        fmt.Println("Running on a secured port")
    
        log.Println("Gateway Running on Port "+port)
        log.Fatal(srv.ListenAndServeTLS(ssl.SSLFullChainPath,ssl.SSLPrivateKeyPath))
        // log.Fatal(http.ListenAndServeTLS(":"+port,ssl.SSLFullChainPath,ssl.SSLPrivateKeyPath,nil))
    }else{
        fmt.Println("Running on an unsecured port")
        fmt.Println("To run on a secured port set up your ssl certificates and add paths "+
            "to SSLFullChain and SSLPrivateKey in the config.json file")
    
        log.Println("Gateway Running on Port "+port)
        log.Fatal(srv.ListenAndServe())
        // log.Fatal(http.ListenAndServe(":"+port, nil))
    }
}