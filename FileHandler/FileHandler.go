package FileHandler

import (
    Network "GOAPIGateway/Network"
    "encoding/base64"
    "encoding/json"
    "net/http"
	"os"
)

type FilePayload struct{
    Name string `json:"name"`
    Payload string `json:"payload"` //base 64 of a file
}

func SaveFile(storageDirectory string,w http.ResponseWriter, r *http.Request){
    var response Network.ErrorResponse
    response.Code = 415
    if(r.Method == "POST"){
        var payload FilePayload
        var file *os.File
        var bts []byte

        decoder := json.NewDecoder(r.Body)
        err := decoder.Decode(&payload)        
        if(err!=nil){
            response.Message = err.Error()
            goto exitCall
        }

        file, err = os.Create(storageDirectory+"/"+payload.Name)
        if err != nil {
            response.Message = err.Error()
            goto exitCall
        }
        defer file.Close()

        bts, err = base64.StdEncoding.DecodeString(payload.Payload)
        if err != nil {
            response.Message = err.Error()
            goto exitCall
        }

        if _, err := file.Write(bts); err != nil {
            response.Message = err.Error()
            goto exitCall
        }

        if err := file.Sync(); err != nil {
            response.Message = err.Error()
            goto exitCall
        }

        response.Code = 200
        response.Message = "File saved succesfully"
        response.Body = storageDirectory+"/"+payload.Name 
    }else{
        response.Message = "Method not supported"
    }

    exitCall:
    json.NewEncoder(w).Encode(response)
}