package URLS

import (
	"fmt"
	"encoding/json"
)

/*
    Supported URLS
*/
type URL struct{
    Source string `json:"source"`
    Method string `json:"method"`
    Target string `json:"target"`
    Secured string  `json:"secured"`
}

func ProcessUrls(mappedUrls map[string]URL,urlSet string)(map[string]URL){
    fmt.Println("Processing URLS")
    var urls []URL
    mappedUrls = map[string]URL{}


    json.Unmarshal([]byte(urlSet),&urls)

    for x := range urls{
        if _,ok := mappedUrls[urls[x].Source];ok{
            fmt.Println(" - The url "+urls[x].Source+" appears multiple tiles");
        }else{
            mappedUrls[urls[x].Source]=urls[x]
        }
    }

    return mappedUrls
}
