package SSL

import (
	"fmt"
	"encoding/json"
)

/*
    Supported URLS
*/
type SSLPayload struct{
    SSLFullChainPath string `json:"SSLFullChainPath"`
    SSLPrivateKeyPath string `json:"SSLPrivateKeyPath"`
}

func ProcessSSL(SSLs SSLPayload,sslPaths string)SSLPayload{
    fmt.Println("Processing SSL")

    json.Unmarshal([]byte(sslPaths),&SSLs)

    return SSLs
}
