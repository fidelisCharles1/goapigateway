package Files

import (
	"os"
	"time"
	"fmt"
    "io/ioutil"
    "strconv"
)

/*
    Since the gateway will be responsible for handling files, we have to create a storage directory to maintain the said files
*/
func SetUpStorageEnviroment(storagePath string)error{
    fmt.Println("Setting up storage enviroment")
    _, err := os.Stat(storagePath)
    if os.IsNotExist(err) { 
        err = nil
        err = os.Mkdir(storagePath, 0755)
    }else if( err != nil ){
        goto ret
    }

    ret:
    return err
}

/*
    This sets up the directory and logs file for the day,
    This file remains open for the reminder of the day and will only be closed when 
    the day has ended and a new request comes in, this reduces latency of opening and closing the file
    hence releasing resources
*/
func SetUpLogEnviroment(directoryPath string)(*os.File,string,error){
    fmt.Println("Setting up logs enviroment")
    var file *os.File
    var fileName string
    _, err := os.Stat(directoryPath)
    if os.IsNotExist(err) { 
        os.Mkdir(directoryPath, 0755)
    }else if( err != nil ){
        goto ret
    }

    fileName = "access-logs-"+time.Now().Format("2006-01-02")+".txt"

    file, err = os.OpenFile(directoryPath+"/"+fileName,os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)    
    if(err!=nil){
        goto ret
    }

    ret:
    return file,fileName,err
}


var DataPool []string


func PersistanceLoading(file *os.File,sizeLimit *int,logsDirectory,logFileName *string){
    var tmp []string = DataPool
    DataPool  = []string{}
    for x := range tmp{
        writeToFile(file,tmp[x])
    }

    fileClosed,err := archiveLogFile(file,*sizeLimit,*logsDirectory)
    if(err!=nil){
        fmt.Println("ERROR ON ARCHIVING FILE")
        fmt.Println(err)        
    }

    if(fileClosed){
        file,*logFileName,err = SetUpLogEnviroment(*logsDirectory)        
    }else{

        //only run thi check if the file is not a new one
        var fileName = "access-logs-"+time.Now().Format("2006-01-02")
        fileName += ".txt"

        //if the logfile names are not similar then it means a different day has started,
        //we hence close the previous file and pull create a new one
        if(fileName != *logFileName){
            //close the previous file
            file.Close()
            //set up a new file
            file,*logFileName,err = SetUpLogEnviroment(*logsDirectory)
            if(err!=nil){
                fmt.Println("ERROR ON SETTING UP NEW FILE FILE")
                fmt.Println(err)        
            }
        }


    }



    time.Sleep(time.Second*30)
    PersistanceLoading(file,sizeLimit,logsDirectory,logFileName)
}

func writeToFile(file *os.File,dataToWrite string)error{
    var err error
    const endLog = "-----------------------------------------------------------------------------"
   	dataToWrite += "\r\n"+endLog+"\r\n"

    _,err = file.WriteString(dataToWrite)
    if(err!=nil){
        goto ret
    }
    err = file.Sync()
    if(err!=nil){
        goto ret
    }

    ret:
    return err
}

//This function archives the current file and creates a new one in it's place for continued logging 
func archiveLogFile(file *os.File,sizeLimit int,logsDirectory string)(bool,error){
    var err error
    var files []os.FileInfo
    var directoryName = logsDirectory+"/archives/"+time.Now().Format("2006-01-02")
    var currentFileName = "access-logs-"+time.Now().Format("2006-01-02")+".txt"
    var currentFileSize int
    var fileClosed bool
    var fileStat os.FileInfo

    fileStat,err = file.Stat()
    if(err!=nil){
        goto ret
    }

    currentFileSize = int( fileStat.Size() )

    if( currentFileSize >= sizeLimit ){
        //we set up archives directory first
        //as the system fails when we jump the middle directory
        err = SetUpStorageEnviroment(logsDirectory+"/archives")
        if(err!=nil){
            goto ret
        }

        err = SetUpStorageEnviroment( directoryName )
        if(err!=nil){
            goto ret
        }

        files,err = ioutil.ReadDir(directoryName)

        file.Close()
        fileClosed = true
        err = os.Rename( logsDirectory+"/"+currentFileName , directoryName+"/file-"+strconv.Itoa(len(files)+1)+".txt" )
        if(err!=nil){
            goto ret
        }
    }



    ret:
    return fileClosed,err
}
