This is a simple and lightweight API gateway created using the Golang programming language

This API gateway comes integrated with JWT, and an ability to secure specific routes, ability to drop/add headers.

In the future i will add 
1. IP filtering 
2. Requests bundling (Making 1 API call that will make this gateway call multiple endpoints and group responses)
3. Logging
4. Rate  Limiting
5. Simple Analytics

Issues 
1. forwarding of formdata and url-encoded-form-data payloads (Pending/Partially working)